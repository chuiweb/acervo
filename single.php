<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="single-produtos">
	<div class="container">
		<div class="col-xs-12 col-sm-6 col-md-6">
			<div class="owl-carousel owl-theme" id="slide-produtos">
				<?php
				$count = 1;
				    $images = acf_photo_gallery('galeria_produto', $post->ID);
				    if( count($images) ):
				        foreach($images as $image):
				            $full_image_url= $image['full_image_url'];
				?>
			    <div class="item" data-hash="slide<?php echo $count; ?>">
			    	<img src="<?php echo $full_image_url; ?>">
			    </div>
				<?php $count++; endforeach; endif; ?>
			</div>

			<?php
			$contador = 1;
			    $images = acf_photo_gallery('galeria_produto', $post->ID);
			    if( count($images) ):
			        foreach($images as $image):
			            $full_image_url= $image['full_image_url'];
			?>
			<div class="col-xs-4 col-md-3 col-slider">
				<a class="button secondary url" href="#slide<?php echo $contador; ?>">
					<img src="<?php echo $full_image_url; ?>">
				</a>
			</div>
			<?php $contador++; endforeach; endif; ?>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-6 col-descricao">
			<?php 
				$terms = get_the_terms( $post->ID , 'categoria' );
				foreach ( $terms as $term ) {} 
			?>
			<a class="link-catalogo" href="<?php echo site_url(); ?>/categoria/<?php echo $term->slug; ?>">
				<span>CATÁLOGO / <?php echo $term->name; ?></span>
			</a>
			<h5><?php the_title(); ?></h5>
			<?php the_content(); ?>
			<hr>
			<div class="infos-produto">
				<p>Medidas: <?php the_field("medida_produto"); ?></p>
				<br>
				<p>Preço: <?php the_field("preco_produto"); ?></p>
			</div>
			<div class="btn-single-produto">
				<a href="https://wa.me/55" target="_blank">clique aqui</a>
			</div>
		</div>
	</div>

	<div class="produtos-relacionados">
		<div class="container">
			<h2>RELACIONADOS</h2>
			<?php $new_query = new WP_Query( 
				array( 
					'posts_per_page' => 3, 
					'post_type' => 'produtos',
					'orderby' => 'rand',
					'tax_query' => array(
				        array(
				            'taxonomy' => 'categoria',
				            'field'    => 'slug',
				            'terms'    => $term->slug,
				        ),
				    ),
				) 
			);
			if( $new_query->have_posts() ) :
				while( $new_query->have_posts() ): $new_query->the_post(); ?>
					<div class="col-xs-12 col-sm-4 col-md-4 col-produtos">
						<a href="<?php the_permalink(); ?>">
							<?php if ( has_post_thumbnail() ) { ?>
								<img src="<?php the_post_thumbnail_url(); ?>">
							<?php } else { ?>
								<img src="<?php echo bloginfo("template_url"); ?>/img/produto.png">
							<?php } ?>
							<h5><?php the_title(); ?></h5>
						</a>
					</div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</div>

<?php endwhile; endif; ?>
	
<?php get_footer(); ?>