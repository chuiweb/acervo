<?php

function admin_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/admin
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'admin' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'admin' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'admin-featured-image', 2000, 1200, true );

	add_image_size( 'admin-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'admin' ),
		'social' => __( 'Social Links Menu', 'admin' ),
	) );
}
add_action( 'after_setup_theme', 'admin_setup' );

$labels = array(
	'name'                => "Produtos",
	'singular_name'       => "Produtos",
	'menu_name'           => "Produtos",
	'name_admin_bar'      => "Produtos"
);

$args = array(
	'labels'              => $labels,
	'supports'            => array('title', 'editor', 'thumbnail'),
	'hierarchical'        => false,
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'menu_icon'           => 'dashicons-screenoptions',
	'show_in_admin_bar'   => true,
	'show_in_nav_menus'   => true,
	'can_export'          => true,
	'has_archive'         => false,
	'exclude_from_search' => true,
	'publicly_queryable'  => true,
	'capability_type'     => 'page',
);

register_post_type( 'produtos', $args );

add_action( 'init', 'categoriaproduto' );
function categoriaproduto() {
	register_taxonomy(
		'categoria',
		'produtos',
		array(
			'label' => __( 'Categoria' ),
			'rewrite' => array( 'slug' => 'categoria' ),
			'hierarchical' => true,
		)
	);
}

function my_login_logo_one() { 
	?> 
	<style type="text/css"> 
	body.login div#login h1 a {
	 background-image: url(<?php echo bloginfo("template_url"); ?>/img/logo.png);
	padding-bottom: 30px; 
	} 
	</style>
	 <?php 
} 
add_action( 'login_enqueue_scripts', 'my_login_logo_one' );

add_action('pre_get_posts', 'archive_paginations');
function archive_paginations($query){
	 if ( !is_admin() && $query->is_archive() && $query->is_main_query() ) {
	 	global $wp_query;
		$cat = $wp_query->get_queried_object();
       $query->set( 'post_type', array( 'produtos' ) );
       $query->set( 'posts_per_page', '1' );
       
       $query->set( 'categoria', $cat->slug );

  }
  return $query;
}

show_admin_bar(false);

?>