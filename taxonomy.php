<?php get_header(); ?>

<?php $term = get_queried_object(); ?>

<div class="page-taxonomy">
	<div class="container">
		<div class="col-xs-12 col-titulos">
			<h2><?php echo $term->name; ?></h2>
		</div>
	</div>

	<div class="container">
		<?php
		    $args = array(
		        'post_type' => 'produtos',
		        'posts_per_page' => 9,
		        'order'	=>	'DESC',
		        'paged' => get_query_var('paged'),
		        'tax_query' => array(
			        array(
			            'taxonomy' => 'categoria',
			            'field'    => 'slug',
			            'terms'    => $term->slug,
			        ),
			    ),
		    );

		    $post_query = new WP_Query($args);
			if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
		?>
		<div class="col-xs-12 col-sm-6 col-md-4 col-produtos">
			<a href="<?php the_permalink(); ?>">
				<div class="mask">
					<?php if ( has_post_thumbnail() ) { ?>
						<img src="<?php the_post_thumbnail_url(); ?>">
					<?php } else { ?>
						<img src="<?php echo bloginfo("template_url"); ?>/img/produto.png">
					<?php } ?>
				</div>
				<h5><?php the_title(); ?></h5>
			</a>
		</div>
		<?php } } ?>
	</div>
	<?php
		wp_pagenavi( array( 'query' => $post_query ) ); 
	?>
	<?php wp_reset_postdata(); ?>
</div>
	
<?php get_footer(); ?>