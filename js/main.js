$(document).ready(function(){

	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();

	    if (scroll >= 100) {
	        $(".col-logo-header").addClass("menu-scroll");
	        $(".navbar-default").css("height", "130px");
	        $("div#bs-example-navbar-collapse-1").css("border", "none");
	    }
	    if (scroll < 100) {
	        $(".col-logo-header").removeClass("menu-scroll");
	        $(".navbar-default").css("height", "160px");
	        $("div#bs-example-navbar-collapse-1").css("border-bottom", "1px solid #eee");
	    }
	}); 

	$(".menu-catalogo a.link-catalogo").click(function(){
		event.preventDefault();
		$(".sub-menu-catalogo").toggleClass("menu-open");
		$("header .menu-catalogo a i").toggleClass("icon-change");
	});

	$("header .col-busca a").click(function(){
		event.preventDefault();
		$("header .campo-busca").toggleClass("open-field");
	});

	$('#slide-produtos').owlCarousel({
        items:1,
        loop:false,
        center:true,
        margin:10,
        URLhashListener:true,
        autoplayHoverPause:true,
        startPosition: 'URLHash',
        nav : true,
        navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'
      ],
    });

	$(".single-produtos .col-slider a.button:first").addClass("button-active");
    $(".single-produtos .col-slider a.button").click(function(){
		$(".single-produtos .col-slider a.button").removeClass("button-active");
		$(this).addClass("button-active");
	});



});