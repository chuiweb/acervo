<?php get_header(); ?>

<div class="banner-destaque">
	<?php 
		$banner = get_field("imagem_principal");
	?>
	<div class="banner-home" style="background: url(<?php echo $banner["sizes"]["large"]; ?>">
		<div class="container">
			<div class="col-xs-12 col-sm-6 col-md-7"></div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="box-texto">
				<h2><?php echo the_field("texto_destaque"); ?></h2>
					<div class="btn-banner">
						<a href="<?php echo the_field("link_botao_destaque"); ?>">DESCUBRA</a>
					</div>
				</div>
			</div>
			<!--<img class="filter" src="<?php //echo bloginfo("template_url"); ?>/img/filter.png">-->
		</div>
	</div>
</div>

<div class="categorias-home">
	<div class="container">
		<h2>Catálogo</h2>
		<?php 
		$tax_produtos = get_terms('categoria');
		foreach ($tax_produtos as $taxonomia) {
			$args = array(
		        'post_type' => 'produtos',
		        'posts_per_page' => 3,
		        'tax_query' => array(
			        array(
			            'taxonomy' => 'categoria',
			            'field'    => 'slug',
			            'terms'    => $taxonomia->slug,
			        ),
			    ),
		    );
		    ?>
		    	<div class="col-xs-12 col-sm-4 col-md-4 col-categorias-home">
					<a href="<?php echo site_url(); ?>/categoria/<?php echo $taxonomia->slug; ?>">
						<img src="<?php echo bloginfo("template_url"); ?>/img/categorias/<?php echo $taxonomia->slug; ?>.png">
						<span><?php echo $taxonomia->name; ?></span>
					</a>
				</div>
		<?php } ?>
	</div>
</div>

<div class="section-imagem-home"></div>

<div class="produtos-home">
	<div class="container">
		<?php
		    $args = array(
		        'post_type' => 'produtos',
		        'posts_per_page' => 6,
		        'order'	=>	'DESC'
		    );

		    $post_query = new WP_Query($args);
			if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
		?>
		<div class="col-xs-12 col-sm-6 col-md-4 col-produtos-home">
			<a href="<?php the_permalink(); ?>">
				<div class="mask">
					<?php if ( has_post_thumbnail() ) { ?>
						<img src="<?php the_post_thumbnail_url(); ?>">
					<?php } else { ?>
						<img src="<?php echo bloginfo("template_url"); ?>/img/produto.png">
					<?php } ?>
				</div>
				<h5><?php the_title(); ?></h5>
			</a>
		</div>
		<?php } } wp_reset_postdata(); ?>
	</div>
</div>

<?php  
	$bannerdestaque = get_field("imagem_produto_destaque");
?>
<div class="banner-produto-destaque" style="background: url(<?php echo $bannerdestaque["sizes"]["large"]; ?>">
	<div class="col-xs-3"></div>
	<div class="col-xs-6">
		<div class="box-texto">
			<?php 
				$tituloprod = get_field("titulo_produto_destaque"); 
				$textoprod = get_field("texto_produto_destaque"); 
				$linkprod = get_field("link_botao_produto_destaque"); 
			?>
			<?php if ($tituloprod) { ?>
				<h2><?php echo the_field("titulo_produto_destaque"); ?></h2>
			<?php } ?>
			<?php if ($textoprod) { ?>
				<?php echo the_field("texto_produto_destaque"); ?>
			<?php } ?>
			<?php if ($linkprod) { ?>
				<div class="btn-banner">
					<a href="<?php echo the_field("link_botao_produto_destaque"); ?>">clique aqui</a>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="col-xs-3"></div>
	<img class="filter" src="<?php echo bloginfo("template_url"); ?>/img/filter.png">
</div>

<?php get_footer(); ?>