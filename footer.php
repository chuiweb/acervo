	<footer>
		<div class="container">
			<div class="col-xs-12 col-md-4">
				<div class="redes-sociais">
					<ul>
						<li>
							<a href="#" target="_blank">
								<i class="fab fa-instagram"></i>
							</a>
						</li>
						<li>
							<a href="#" target="_blank">
								<i class="fab fa-whatsapp" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="mailto:contato@acervoaberto.com.br" target="_blank">
								<i class="far fa-envelope"></i>
							</a>
						</li>
					</ul>
					<p>Alameda Princesa Izabel, 359 - Mercês - Curitiba - PR<br>
					Atendimento de Segunda à Sexta-feira das 10h às 18h </p>
				</div>
			</div>
			<div class="col-xs-12 col-md-2"></div>
			<div class="col-xs-12 col-md-6">
				<img class="logo-footer" src="<?php echo bloginfo("template_url"); ?>/img/logo-footer.svg">
			</div>
		</div>
	</footer>	



	<script type="text/javascript" src="<?php echo bloginfo("template_url"); ?>/js/main.js"></script>
	<?php wp_footer(); ?>
</body>
</html>