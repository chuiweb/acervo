<?php get_header(); ?>

<div class="resultados-busca">
	<div class="container">
		<div class="col-xs-12">
			<h4>Resultados para busca: <?php printf( __( '%s' ), '<span>' . get_search_query() . '</span>' ); ?></h4>
		</div>
		<?php
		    $args = array(
		        'post_type' => 'produtos',
		        'posts_per_page' => -1,
		        'order'	=>	'ASC'
		    );

		    $post_query = new WP_Query($args);
			if($post_query->have_posts() ) { while($post_query->have_posts() ) { $post_query->the_post(); 
		?>
		<div class="col-xs-12 col-sm-6 col-md-4 col-produtos">
			<a href="<?php the_permalink(); ?>">
				<div class="mask">
					<?php if ( has_post_thumbnail() ) { ?>
						<img src="<?php the_post_thumbnail_url(); ?>">
					<?php } else { ?>
						<img src="<?php echo bloginfo("template_url"); ?>/img/produto.png">
					<?php } ?>
				</div>
				<h5><?php the_title(); ?></h5>
			</a>
		</div>
		<?php } } wp_reset_postdata(); ?>
	</div>
</div>

<?php get_footer(); ?>