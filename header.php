<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php bloginfo('name'); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link type="text/css" rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/main.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/bootstrap.min.css"> 
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/animate.min.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo bloginfo("template_url"); ?>/css/owl.carousel.css">
	<link rel='shortcut icon' type='image/x-icon' href='<?php echo bloginfo("template_url"); ?>/img/favicon.png' />
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	
	<script src="<?php echo bloginfo("template_url"); ?>/js/jquery-1.11.3.min.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/owl.carousel.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/owl.hash.js"></script>
	<script src="<?php echo bloginfo("template_url"); ?>/js/smooth-scroll.js"></script>	
	<script src="<?php echo bloginfo("template_url"); ?>/js/wow.js"></script>	

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<header class="header clear" role="banner">
		<nav class="navbar navbar-default">
		  <div class="container">
		    <div class="navbar-header">
		    	<a href="<?php echo site_url(); ?>">
		    		<img class="logo-header hidden-sm hidden-md hidden-lg" src="<?php echo bloginfo("template_url"); ?>/img/logo.svg">
		    	</a>
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar icon-bar-1"></span>
		        <span class="icon-bar icon-bar-2"></span>
		        <span class="icon-bar icon-bar-3"></span>
		      </button>
		    </div>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		    	<div class="col-sm-3 col-md-5 col-logo-header">
			    	<a href="<?php echo site_url(); ?>">
		    			<img class="logo-header hidden-xs" src="<?php echo bloginfo("template_url"); ?>/img/logo.svg">
		    			<img class="logo-header-scroll hidden-xs" src="<?php echo bloginfo("template_url"); ?>/img/logo-internas.svg">
			    	</a>
			    </div>
			    <div class="col-sm-8 col-md-6 col-lg-5">
			      <ul class="nav navbar-nav">
			        <li><a href="<?php echo site_url(); ?>">HOME</a></li>
			        <li class="menu-catalogo">
			        	<a href="#" class="link-catalogo">CATÁLOGO <i class="fas fa-chevron-down"></i></a>
			        	<div class="sub-menu-catalogo">
			        		<ul>
			        			<li><a href="<?php echo site_url(); ?>/categoria/objetos">OBJETOS</a></li>
			        			<li><a href="<?php echo site_url(); ?>/categoria/moveis">MÓVEIS</a></li>
			        			<li><a href="<?php echo site_url(); ?>/categoria/iluminacao">ILUMINAÇÃO</a></li>
			        		</ul>
			        	</div>
			        </li>
			        <li><a href="<?php echo site_url(); ?>/sobre">SOBRE</a></li>
			        <li><a href="<?php echo site_url(); ?>/contato">CONTATO</a></li>
			      </ul>
			  </div>
			  <div class="hidden-xs col-sm-1 col-busca">
			  	<a href="#">
			  		<i class="fa fa-search" aria-hidden="true"></i>
			  	</a>
			  </div>
		  	<div class="campo-busca">
		  		<?php //get_search_form(); ?>
		  		<form role="search" method="get" id="searchform" class="searchform" action="<?php echo site_url(); ?>">
					<div>
						<input type="text" value="" name="s" id="s" placeholder="buscar no acervo">
						<button type="submit" id="searchsubmit"><i class="fa fa-search" aria-hidden="true"></i></button>
					</div>
				</form>
		  	</div>
		  </div>
		</nav>
	</header>
	




