<?php get_header(); ?>

	<div class="page-contato">
		<div class="container">
			<div class="col-xs-12 col-md-6">
				<?php if ( has_post_thumbnail() ) { ?>
					<img class="img-contato" src="<?php the_post_thumbnail_url(); ?>">
				<?php } ?>
			</div>
			<div class="col-xs-12 col-md-5 col-conteudo-contato">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>